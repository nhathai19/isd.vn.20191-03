# Final Report

## Lê Hải Dương

- Design Class/Package Diagram and Data Model
- Write source code for the whole project

## Nguyễn Anh Đức

- Update SDD
- Write test for One-way Ticket

## Lã Ngọc Dương

- Update SRS
- Write test for 24h Ticket

## Nguyễn Nhật Hải

- Write JavaDoc
