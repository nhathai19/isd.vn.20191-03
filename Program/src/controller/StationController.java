package controller;

import model.Station;

import java.sql.*;
import java.util.*;

/**
 * The Class StationController.
 */
public class StationController {

    private static StationController instance = new StationController();

    private Statement stmt = DBConnect.getStmt();

    private List<Station> listStation;

    public static StationController getInstance() {
        return instance;
    }

    /**
     * Gets the list station.
     *
     * @return the list of station
     * @throws SQLException if there was a problem querying the database
     */
    public List<Station> getListStation() throws SQLException {
        listStation = new ArrayList<Station>();
        ResultSet rs = this.stmt.executeQuery("SELECT * FROM station");
        while (rs.next()) {
            listStation.add(new Station(rs.getInt(1), rs.getString(2), rs.getFloat(3)));
        }
        return this.listStation;
    }

    /**
     * Gets the station name.
     *
     * @param id station id
     * @return the station name
     * @throws SQLException if there was a problem querying the database
     */
    public String getStationName(int id) throws SQLException {
        ResultSet rs = this.stmt.executeQuery("SELECT name FROM station WHERE id=" + id);
        rs.next();
        String name = rs.getString("name");
        return name;
    }

    /**
     * Gets the distance.
     *
     * @param id station id
     * @return the distance
     * @throws SQLException if there was a problem querying the database
     */
    public Float getDistance(int id) throws SQLException {
        ResultSet rs = this.stmt.executeQuery("SELECT distance FROM station WHERE id=" + id);
        rs.next();
        Float distance = rs.getFloat("distance");
        return distance;
    }
}