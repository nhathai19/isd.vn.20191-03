package controller;

import model.Ticket24h;
import view.UI;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * The Class Ticket24hController.
 */
public class Ticket24hController extends PlatformController {

	private static Ticket24hController instance = new Ticket24hController();

	private List<Ticket24h> listTicket24h;

	private Statement stmt = DBConnect.getStmt();

	private Ticket24h ticket24h;

	private UI ui = UI.getInstance();

	public static Ticket24hController getInstance() {
		return instance;
	}

	/**
	 * Gets the list ticket 24h.
	 *
	 * @return the list of ticket 24h
	 * @throws SQLException if there was a problem querying the database
	 */
	public List<Ticket24h> getListTicket24h() throws SQLException {
		listTicket24h = new ArrayList<Ticket24h>();
		ResultSet rs = this.stmt.executeQuery("SELECT * FROM ticket_24h");
		while (rs.next()) {
			listTicket24h.add(new Ticket24h(
					rs.getString(1),
					rs.getString(2),
					rs.getString(3),
					rs.getTimestamp(4),
					rs.getString(5)
			));
		}
		return this.listTicket24h;
	}

	/**
	 * Enter station.
	 *
	 * @param enterStationId the enter station id
	 * @param code the code of ticket
	 * @return true, if the passenger can enter, false otherwise
	 * @throws SQLException if there was a problem querying the database
	 */
	public boolean enterStation(int enterStationId, String code) throws SQLException {
		this.setTicket24h(code);

		if (this.ticket24h.getStatus().equals("using")) {
			ui.setMessage("Invalid 24h ticket");
			ui.setError("Ticket is already in use");
			return false;
		}

		if (this.ticket24h.getStatus().equals("expired")) {
			ui.setMessage("Invalid 24h ticket");
			ui.setError("Ticket cannot be used to enter since it was expired");
			return false;
		}

		if (this.ticket24h.getExpireTime() != null) {
			if (this.ticket24h.getExpireTime().compareTo(Timestamp.valueOf(LocalDateTime.now())) > 0) {
				this.stmt.executeUpdate("UPDATE ticket_24h SET status='using' WHERE code='" + code + "'");
				ui.setMessage(null);
				ui.setError(null);
				return true;
			}
			this.stmt.executeUpdate("UPDATE ticket_24h SET status='expired' WHERE code='" + code + "'");
			ui.setMessage("Invalid 24h ticket");
			ui.setError("Ticket cannot be used to enter since it was expired");
			return false;
		}

		Timestamp expireTime = Timestamp.valueOf(LocalDateTime.now().plusDays(1));
		this.stmt.executeUpdate("UPDATE ticket_24h SET status='using', expire_time='" + expireTime + "' WHERE code='" + code + "'");
		ui.setMessage(null);
		ui.setInfo("Valid until " + expireTime);
		ui.setError(null);
		return true;
	}

	/**
	 * Exit station.
	 *
	 * @param exitStationId the exit station id
	 * @param code the code of ticket
	 * @return true, if the passenger can exit, false otherwise
	 * @throws SQLException if there was a problem querying the database
	 */
	public boolean exitStation(int exitStationId, String code) throws SQLException {
		this.setTicket24h(code);

		if (this.ticket24h.getStatus().equals("ready")) {
			ui.setMessage("Invalid 24h ticket");
			ui.setError("Ticket is not used to enter a station before");
			return false;
		}

		if (this.ticket24h.getStatus().equals("expired")) {
			ui.setMessage("Invalid 24h ticket");
			ui.setError("Ticket cannot be used to enter since it was expired");
			return false;
		}

		if (this.ticket24h.getExpireTime().compareTo(Timestamp.valueOf(LocalDateTime.now())) < 0) {
			this.stmt.executeUpdate("UPDATE ticket_24h SET status='expired' WHERE code='" + code + "'");
			ui.setMessage(null);
			ui.setError("Ticket is now expired");
			return true;
		}

		this.stmt.executeUpdate("UPDATE ticket_24h SET status='ready' WHERE code='" + code + "'");
		ui.setMessage(null);
		ui.setError(null);
		return true;
	}

	/**
	 * Sets the ticket 24 h.
	 *
	 * @param code the new ticket 24h
	 * @throws SQLException if there was a problem querying the database
	 */
	private void setTicket24h(String code) throws SQLException {
		ResultSet rs = this.stmt.executeQuery("SELECT * FROM ticket_24h WHERE code='" + code + "'");
		rs.next();
		this.ticket24h = new Ticket24h(
				rs.getString(1),
				rs.getString(2),
				rs.getString(3),
				rs.getTimestamp(4),
				rs.getString(5)
		);
	}
}