# README

## Lê Hải Dương

- Draw the sequence diagrams for: Enter the station platform using one-way ticket
- Re-design use-case diagram
- Summarize member's works

## Nguyễn Anh Đức

Design sequency diagram for use case

- Exit using one-way ticket

## Nguyễn Nhật Hải

Làm sequence diagram cho usecase:

- Enter using pre-paid card

- Exit using pre-paid card

## Lã Ngọc Dương

Làm sequence diagram cho usecase:

- Enter using 24h ticket

- Exit using 24h ticket
